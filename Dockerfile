FROM gitlab-registry.cern.ch/dmonk-emp-projects/serenity-docker-images/serenity-sw:latest

COPY ./requirements.txt /requirements.txt
RUN python3 -m pip install -r requirements.txt

COPY ./app /app
WORKDIR /app
CMD ["bash", "run.sh"]
