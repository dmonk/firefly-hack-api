from typing import Union
from fastapi import FastAPI

import subprocess


app = FastAPI()


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.post("/configure")
def configure():
    subprocess.run("serenitybutler fireflys configure --cdr disable --invert-rx J0:6", shell=True)
    return {"status": "complete"}
